#Задача: Створіть дві змінні first=10, second=30.
#Виведіть на екран результат математичної взаємодії (+, -, *, / и тд.) для цих чисел.

testVarFirst = 31
testVarSecond = 10

result = testVarFirst + testVarSecond
print(result)

result = testVarFirst - testVarSecond
print(result)

result = testVarFirst * testVarSecond
print(result)

result = testVarFirst / testVarSecond
print(result)

result = testVarFirst ** testVarSecond  #power
print(result)

result = testVarFirst // testVarSecond  #devision with round
print(result)

result = testVarFirst % testVarSecond   #remainder of the division
print(result)

#Задача: Створіть змінну і запишіть в неї результат порівняння (<, > , ==, !=) чисел з завдання 1.
#Виведіть на екран результат кожного порівняння.

boolResult = testVarFirst > testVarSecond
print(boolResult)

boolResult = testVarFirst < testVarSecond
print(boolResult)

boolResult = (testVarFirst % testVarSecond) == (testVarFirst - (testVarSecond * (testVarFirst // testVarSecond)))
print(boolResult)

boolResult = (2+2) != 5
print(boolResult)

#Задача: Створіть змінну - результат конкатенації (складання) строк
#str1="Hello " и str2="world". Виведіть на екран.

oneSaid = "Glory to Ukraine!"

thousandsReplied = "Glory to the heroes!"

readyMessage = oneSaid + "\n" + thousandsReplied

print(readyMessage)
