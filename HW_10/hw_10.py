class Vehicle:
    vehicle_type = "vehicle"
    sound = "default sound of any vehicle"

    def saund(self):
        return self.sound


class Car(Vehicle):
    wheel = False

    def __init__(self, vehicle_type, sound="brrrr-rrrrr-rrrrr"):
        self.vehicle_type = vehicle_type
        self.sound = sound

    def inflate_wheel(self):
        print("I am inflating the wheel")
        self.wheel = True


class Plane(Vehicle):
    ice = True

    def __init__(self, vehicle_type, sound="iuuuuuuuuuuuuuuuuu-uuuuuuuuuu"):
        self.vehicle_type = vehicle_type
        self.sound = sound

    def clear_ice(self):
        print("I am clearing the ice")
        self.ice = False


class Ship(Vehicle):
    sales = False
    sales_color = "white"

    def __init__(self, vehicle_type, sound="wshhhhh-wshhhhh", sales_color="white"):
        self.vehicle_type = vehicle_type
        self.sound = sound
        self.sales_color = sales_color

    def saund(self):
        return f'{self.sound} and {self.sales_color} sales'


ship = Ship("sale_ship")

print(ship.vehicle_type)

car = Car("toy_car")
print(f'{car.vehicle_type} make sound: {car.saund()}')

vehicles = [Ship("sale_ship"), Car("rice_car"), Plane("AirBus A310")]
vehicles.append(Ship(vehicle_type="just_ship", sales_color="Red"))

print("---------------------------------")


def see_all_classes():
    for vehicle in vehicles:
        print(f'{vehicle.vehicle_type} make {vehicle.saund()} sound')

        if type(vehicle) is Car:
            if (not vehicle.wheel):
                print(f'oh, {vehicle.vehicle_type} have traubles with wheel')
                vehicle.inflate_wheel()
            else:
                print(f'{vehicle.vehicle_type} can go! wheels are okay')

        print("---------------------------------")


see_all_classes()
see_all_classes()
