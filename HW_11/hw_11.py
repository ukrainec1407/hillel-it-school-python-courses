import copy

class Point:
    _x = None
    _y = None

    def __init__(self, dot_x=0, dot_y=0):
        if (isinstance(dot_x, int | float)
                and isinstance(dot_y, int | float)):
            self._x = dot_x
            self._y = dot_y
        else:
            raise TypeError

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, dot_x):
        if (isinstance(dot_x, int | float)):
            self._x = dot_x
        else:
            raise TypeError

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, dot_y):
        if (isinstance(dot_y, int | float)):
            self._y = dot_y
        else:
            raise TypeError

class Shape:
    _tops = []

    def shape_decorator(self, top_points):
        if(isinstance(top_points, list)):
            return True
        else:
            raise TypeError

    # how to make @shape_decorator for __init__?
    def __init__(self, top_points):
        if (self.shape_decorator(top_points)):
            for top in top_points:
                if(not isinstance(top, Point)):
                    raise TypeError
            self._tops = copy.deepcopy(top_points)
        else:
            raise TypeError

    @property
    def tops(self):
        return self._tops


    @tops.setter
    def tops(self, top_points):
        if (self.shape_decorator(top_points)):
            for top in top_points:
                if(not isinstance(top, Point)):
                    raise TypeError
            self._tops = copy.deepcopy(top_points)
        else:
            raise TypeError

    @property
    def area(self):
        xy_sum = 0
        yx_sum = 0
        for i in range(0, len(self._tops) - 1):
            xy_sum += (self._tops[i].x * self._tops[i+1].y)
        xy_sum += (self._tops[-1].x * self._tops[0].y)

        for i in range(0, len(self._tops) - 1):
            yx_sum += (self._tops[i].y * self._tops[i+1].x)
        yx_sum += (self._tops[-1].y * self._tops[0].x)

        result = abs(xy_sum - yx_sum) / 2

        if(result == 0 and len(self._tops) == 0):
            result = ((self._tops[1].y - self._tops[0].y) * (self._tops[3].x - self._tops[0].x)) ** 2

        return result

