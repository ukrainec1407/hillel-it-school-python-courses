import copy

class Point:
    _x = None
    _y = None

    def __init__(self, dot_x=0, dot_y=0):
        if (isinstance(dot_x, int | float)
                and isinstance(dot_y, int | float)):
            self._x = dot_x
            self._y = dot_y
        else:
            raise TypeError

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, dot_x):
        if (isinstance(dot_x, int | float)):
            self._x = dot_x
        else:
            raise TypeError

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, dot_y):
        if (isinstance(dot_y, int | float)):
            self._y = dot_y
        else:
            raise TypeError

class Shape:
    _tops = []

    @staticmethod
    def shape_decorator(func):
        def wrapper(*top_points):
            if(len(top_points) == 2) and (isinstance(top_points[1], list)):
                res = func(top_points[0], top_points[1])
            else:
                raise TypeError
            return res
        return wrapper

    @staticmethod
    def magic_decorator(func):
        def wrapper(*args):
            for arg in args:
                if(not isinstance(arg, Shape)):
                    raise TypeError
            res = func(args[0], args[1])
            return res
        return wrapper

    @shape_decorator
    def __init__(self, top_points):
        for top in top_points:
            if (not isinstance(top, Point)):
                raise TypeError
        self._tops = copy.deepcopy(top_points)

    @property
    def tops(self):
        return self._tops

    @tops.setter
    def tops(self, top_points):
        if (self.shape_decorator(top_points)):
            for top in top_points:
                if(not isinstance(top, Point)):
                    raise TypeError
            self._tops = copy.deepcopy(top_points)
        else:
            raise TypeError

    @property
    def get_area(self):
        xy_sum = 0
        yx_sum = 0
        for i in range(0, len(self._tops) - 1):
            xy_sum += (self._tops[i].x * self._tops[i+1].y)
        xy_sum += (self._tops[-1].x * self._tops[0].y)

        for i in range(0, len(self._tops) - 1):
            yx_sum += (self._tops[i].y * self._tops[i+1].x)
        yx_sum += (self._tops[-1].y * self._tops[0].x)

        result = abs(xy_sum - yx_sum) / 2

        if(result == 0 and len(self._tops) == 0):
            result = ((self._tops[1].y - self._tops[0].y) * (self._tops[3].x - self._tops[0].x)) ** 2

        return result

    @magic_decorator
    def __lt__(self, other):
        if (self.get_area < other.get_area):
            return True
        else:
            return False

    @magic_decorator
    def __le__(self, other):
        if (self.get_area <= other.get_area):
            return True
        else:
            return False

    @magic_decorator
    def __eq__(self, other):
        if (self.get_area == other.get_area):
            return True
        else:
            return False

    @magic_decorator
    def __ne__(self, other):
        if (self.get_area != other.get_area):
            return True
        else:
            return False

    @magic_decorator
    def __ge__(self, other):
        if (self.get_area >= other.get_area):
            return True
        else:
            return False

    @magic_decorator
    def __gt__(self, other):
        if (self.get_area > other.get_area):
            return True
        else:
            return False

    def __str__(self):
        points = ""
        counter = 1
        for top in self.tops:
            points += f"X{counter} = {top.x}, Y{counter} = {top.y}; "
            counter += 1
        points += f"Area of shape is: {self.get_area}"

        return points

point_1 = Point(0, 0)
point_2 = Point(1, 0)
point_3 = Point(0, 1)

point_4 = Point(0, 0)
point_5 = Point(2, 0)
point_6 = Point(3, 3)
point_7 = Point(0, 2)

shape_1 = Shape([point_1, point_2, point_3])

shape_2 = Shape([point_4, point_5, point_6, point_7])

print(str(shape_1))

print(str(shape_2))

assert (shape_1 > shape_2) is False
assert (shape_1 < shape_2) is True
assert (shape_1 == shape_2) is False
assert (shape_1 != shape_2) is True


