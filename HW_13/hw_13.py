import requests

def nbu_to_txt(file_path):
    url = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
    error = ""

    try:
        result = requests.get(url)
    except Exception as inst:
        result = None
        error = str(inst)

    try:
        result = result.json()
    except Exception as inst:
        result = None
        error = str(inst)

    if (result != None):
        counter = 1
        with open('file_path', 'w') as file:
            file.write(f"{result[0]['exchangedate']}\n")
            for res in result:
                file.write(f"{counter}. {res['txt']} to UAH: {res['rate']}\n")
                counter += 1
    else:
        with open('file_path', 'w') as file:
            file.write(error)

nbu_to_txt('nbu_to_txt')