# Зформуйте строку, яка містить певну інформацію про символ по його індексу в відомому слові.
# Наприклад "The [індекс символу] symbol in [тут слово] is '[символ з відповідним порядковим номером]'".
# Слово та номер отримайте за допомогою input() або скористайтеся константою.
# Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in "Python" is 't' ".

print("-----------------1 Task--------------------")

input_string = input('Введи свою строку\n')

while len(input_string) == 0:
    input_string = input('Введена строка пуста, введіть ще раз\n')

input_index = input('Введи номер символу із цієї строки\n')

while True:
    if input_index.lstrip("-").isdecimal():
        if abs(int(input_index)) <= len(input_string) and abs(int(input_index)) != 0:
            break
        else:
            input_index = input('Введений індекс виходить не входить в межі введеної строки.\n')
    else:
        input_index = input('Введений номер не є цифрою. Введіть його правильно будь ласка.\n')

input_index = int(input_index)
print(f'{input_index} символ в "{input_string}" - це \'{input_string[input_index - 1]}\'')

print("-----------------2 Task--------------------")

# Вести з консолі строку зі слів за допомогою input() (або скористайтеся константою).
# Напишіть код, який визначить кількість слів, в цих даних.

# input_string = input('Введіть своє речення\n')
# while len(input_string) == 0:
#     input_string = input('Введена строка пуста, введіть ще раз\n')
#
# words_counter = 1
# position_counter = 0
#
# input_string = ' '.join(input_string.split())
#
# for symbol in input_string:
#     if symbol == " ":
#         if position_counter != 0 and position_counter != (len(input_string) - 1):
#             words_counter += 1
#     position_counter += 1

#print(words_counter)

input_string = input('Введіть своє речення\n')

print(f'слів у реченні {len(input_string.split())}')

print("-----------------3 Task--------------------")

# Існує ліст з різними даними, наприклад lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть код, який сфoрмує новий list (наприклад lst2), який би містив всі числові змінні (int, float), які є в lst1.
# Майте на увазі, що данні в lst1 не є статичними можуть змінюватись від запуску до запуску.

first_list = ['-1,2', 2.2, -3, True, 'Falser', 5, '6,,2', 7, 8, True, 'Python', 9, 0, 'Lorem Ipsum']

second_list = []

print(first_list)

for list_item in first_list:
    if type(list_item) is int or type(list_item) is float:
        second_list.append(list_item)
    # elif type(list_item) is str:
    #     if str(list_item).lstrip("-").replace(".", "", 1).replace(",", "", 1).isdecimal():
    #         second_list.append(list_item)

print(second_list)

