print("-----------------1 Task--------------------")

# Дана довільна строка. Напишіть код, який знайде в ній і віведе на екран кількість слів,
# які містять дві голосні літери підряд.

input_string = input('Enter your sentence\n')

while True:
    input_string.lower()
    input_string = ' '.join(input_string.split())
    if len(input_string) == 0:
        input_string = input('The entered line is empty, enter again\n')
        continue
    break

print(f"Input string: \"{input_string}\"")

words = input_string.split()
vowel_letters = "aeiou"
vowel_letters_words = 0

#2 or more vowel_letters in a row
# for word in words:
#     last_letter = " "
#     for letter in word:
#         if letter in vowel_letters and last_letter in vowel_letters:
#             vowel_letters_words += 1
#             break
#         else:
#             last_letter = letter
#2 or more vowel_letters in a row

#only 2 vowel_letters in a row
for word in words:
    last_letter = " "
    already_two = False
    already_found = False

    for letter in word:
        if letter in vowel_letters and last_letter in vowel_letters:
            if already_two:
                vowel_letters_words -= 1
                break
            else:
                already_two = True
                if already_found == False:
                    vowel_letters_words += 1
                    already_found = True
        else:
            already_two = False
            last_letter = letter
#only 2 vowel_letters in a row

print(f"{vowel_letters_words} words with 2 in a row vowel letters in your sentence")

print("-----------------2 Task--------------------")

#Є два довільних числа які відповідають за мінімальну і максимальну ціну.
# Є Dict з назвами магазинів і цінами:
# { "cito": 47.999, "BB_studio" 42.999, "momo": 49.999, "main-service": 37.245,
#   "buy.now": 38.324, "x-store": 37.166, "the_partner": 38.988, "store": 37.720, "rozetka": 38.003}.
# Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких попадають в діапазон між мінімальною і максимальною ціною.
# Наприклад:
# lower_limit = 35.9
# upper_limit = 37.339
# > match: "x-store", "main-service"

stores_prices = { "cito": 47.999, "BB_studio": 42.999, "momo": 49.999, "main-service": 37.245, "buy.now": 38.324,
  "x-store": 37.166, "the_partner": 38.988, "store": 37.720, "rozetka": 38.003}

print(stores_prices)

lower_limit = input("write a lower limit of your filter")

while True:
    if lower_limit.isdigit():
        lower_limit = int(lower_limit)
        if lower_limit > 0:
            break
        else:
            lower_limit = input("lower limit have to be a positive")
    else:
        lower_limit = input("lower limit have to be a digit")

upper_limit = input("write an upper limit of your filter")

while True:
    if upper_limit.isdigit():
        upper_limit = int(upper_limit)
        if upper_limit > lower_limit and upper_limit > 0:
            break
        else:
            upper_limit = input("upper limit have to be bigger than lower limit and to be a positive")
    else:
        upper_limit = input("upper limit have to be a digit")

for offer in stores_prices.items():
    if offer[1] > lower_limit and offer[1] < upper_limit:
        print(offer)
        #I am printing not only shop's name, but also price. I think it's better
