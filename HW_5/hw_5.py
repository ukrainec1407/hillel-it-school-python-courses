from pprint import pprint
import requests
import re

print('------------------1 Task--------------------')

# завдання 1
# урл http://api.open-notify.org/astros.json
# вивести список всіх астронавтів, що перебувають в даний момент на орбіті

url = 'http://api.open-notify.org/astros.json'

response = requests.get(url)

astronauts = response.json()['people']

for astronaut in astronauts:
    pprint(f"{astronaut['name']} is on {astronaut['craft']} now")

print('------------------2 Task--------------------')

# завдання 2
# апі погоди (всі токени я для вас вже прописав)
# https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid=47503e85fabbabc93cff28c52398ae97&units=metric
# де city_name - назва міста на аглійській мові (наприклад, odesa, kyiv, lviv, london)
# результатом буде приблизно такий результат
# {"coord":{"lon":30.7326,"lat":46.4775},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04n"}],"base":"stations","main":{"temp":13.94,"feels_like":12.8,"temp_min":13.94,"temp_max":13.94,"pressure":1021,"humidity":54,"sea_level":1021,"grnd_level":1015},"visibility":10000,"wind":{"speed":4.58,"deg":314,"gust":8.16},"clouds":{"all":73},"dt":1664909335,"sys":{"country":"UA","sunrise":1664855942,"sunset":1664897549},"timezone":10800,"id":698740,"name":"Odesa","cod":200}
# погода змінюється, як і місто. яке ви введете
# роздрукувати тепрературу та швидкість вітру. з вказівкою міста, яке було вибране

city_check = re.compile('[A-z]+[-]*[ ]?[A-z]+')

city = input("This is your weather helper.\nEnter the name of the city where you want to find out the weather:\n")

while True:
    city_check_result = re.search(city_check, city)
    if city_check_result is None:
        city = input("Enter the name of the city where you want to find out the weather correctly:\n")
        continue
    city_check_result = city_check_result.group()

    if city == city_check_result:
        print(f"I am processing your request: Weather in the city {city}")
        break
    else:
        city = input("Enter the name of the city where you want to find out the weather correctly:\n")

url = f'https://api.openweathermap.org/data/2.5/weather?q={city}&appid=47503e85fabbabc93cff28c52398ae97&units=metric'

response = requests.get(url)
response = response.json()

if response['cod'] == 200:
    temperature = response['main']['temp']

    wind = response['wind']['speed']

    print(f"Weather in the city: {city}")
    print(f"The wind speed in {response['name']} is {wind} and temperature is {temperature}°C")
else:
    print(f"{city} not found. Please, make sure the input is correct")