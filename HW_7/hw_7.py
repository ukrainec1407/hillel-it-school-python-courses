from pprint import pprint

# ----------------------------TASK 1-----------------------------------

# Є дікт, невідомого наповнення.
# В дікті присутні ключі, занченням для яких є дікти невідомого наповнення в яких можуть бути аналогічні вкладені дікти.
# Напишіть функцію, яка дістане всі ключі зі значеннями не-діктами з усіх рівнів вкладення,
# помістить на один рівень в окремий дікт і поверне цей дікт.

def dict_converter(input_dict):
    """
       This function converts a multilevel dict to a single-level one.
       The keys and values that are not dict are kept as well.
       The number of nested dicts is unlimited.
       Same key names cause data loss.

       :param: input_dict: dict, from which the single-level dict will be assembled
       :type: input_dict: dict

       :return: a single-level dict
       :rtype: dicta
   """

    one_level_dict = {}
    for key, value in input_dict.items():
        if type(value) is dict:
            one_level_dict.update(dict_converter(value))
        else:
            one_level_dict[key] = value
    return one_level_dict


example_dict = {
    "coord": {
        "lon": 34.9833,
        "lat": 48.45
    },
    "weather":
        {
            "id": 800,
            "main": "Clear",
            "description": "clear sky",
            "icon": "01d"
        },
    "base": "stations",
    "main": {
        "temp": 16.91,
        "feels_like": 15.36,
        "temp_min": 16.91,
        "temp_max": 16.91,
        "pressure": 1030,
        "humidity": 27,
        "sea_level": 1030,
        "grnd_level": 1013
    },
    "visibility": 10000,
    "wind": {
        "speed": 3.79,
        "deg": 237,
        "gust": 4.89
    },
    "clouds": {
        "all": 6
    },
    "dt": 1666087075,
    "sys": {
        "country": "UA",
        "sunrise": 1666065825,
        "sunset": 1666104387
    },
    "timezone": 10800,
    "id": 709930,
    "name": "Dnipro",
    "cod": 200
}
# pprint(example_dict)

result = dict_converter(example_dict)
pprint(result)
