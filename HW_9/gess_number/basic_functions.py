import random
import math
import copy


class Answer:
    """
    class Answer created to contain result of functions work
    errors[] - errors during work
    answer_data[] - result data
    """
    errors = []
    answer_data = []

    def __init__(self, errors, data):
        self.errors = errors
        self.answer_data = data


def check_input(*input_data):
    """
       This function check numbers on correct.
       It returns list of data in INT type and list of errors in STR type.(answer_data and errors)
       It gets 1 or more parameters, which we need to check and convert on integer.
       It's allowed to set as parameter str, list, tuple, (int, float).
       In case you use str, list, tuple as parameters - it have to be only one parameter.
       If you use STR - numbers have to be separated by SPACE


       :param: input_data: data to check and convert to list of INT
       :type: input_data: str, list, tuple, int, float

       :return: Answer class which have (answer_data[] and errors[]) fields
       :rtype: Answer (answer_data[] and errors[])
    """

    errors = []
    ready_data = []

    if(len(input_data) > 0):
        if(isinstance(input_data[0], list)):
            input_data = input_data[0]
        elif(isinstance(input_data[0], tuple)):
            input_data = input_data[0]
        elif(isinstance(input_data[0], str)):
            input_data = input_data[0].split()
        if(len(input_data) > 0):
            for element in input_data:
                try:
                    input_data = int(element)
                    ready_data.append(input_data)
                except:
                    errors.append(f'"{element}" {messages["errors"]["invalid_input"]}')
        else:
            errors.append(messages["errors"]["invalid_input"])
    else:
        errors.append(messages["errors"]["out_of_range"])
    result = Answer(errors, ready_data)
    return result

def check_number(user_number, target_number, range):
    """
       This function compare numbers and check if users_number inside of range.

       :param: user_number: number, which user enter during trying guess target number
       :type: user_number: int

       :param: target_number: number, which computer generated for guessing
       :type: target_number: int

       :param: range: list with 2 elements (min value of range, and max value)
       :type: range: list (min value of range, and max value)

       :return: Answer class which have (answer_data[] and errors[]) fields
                answer_data[] - consist messages for user (warm, cold and hot)
       :rtype: Answer (answer_data[] and errors[])
    """
    errors = []
    ready_data = []
    rande_grid = abs(range[0]-range[1])/100

    if(range[0] <= user_number and range[1] >= user_number):
        if(user_number == target_number):
            ready_data.append(messages["messages"]["win"])
        else:
            difference = abs(user_number - target_number)
            if difference > math.ceil(rande_grid * 15):
                ready_data.append(messages["messages"]["cold"])
            elif difference <= math.ceil(rande_grid * 15) and difference >= math.ceil(rande_grid * 7):
                ready_data.append(messages["messages"]["warm"])
            elif difference < math.ceil(rande_grid * 7) and difference >= 1:
                ready_data.append(messages["messages"]["hot"])
    else:
        errors.append(messages["errors"]["out_of_range"])

    result = Answer(errors, ready_data)
    return result

def set_number(range):
    """
           This function generates random number in specified range

           :param: range: list with 2 elements (min value of range, and max value)
           :type: range: list (min value of range, and max value)

           :return: Answer class which have (answer_data[] and errors[]) fields
                        answer_data[] - consist int with random number
           :rtype: Answer (answer_data[] and errors[])
    """
    errors = []
    ready_data = []

    ready_data.append(random.randint(range[0], range[1]))

    result = Answer(errors, ready_data)
    return result

messages = {
    "greetings":"""Hello, player!
                    You are welcomed to the game "guess a number".
                    You will have to guess the number the computer guessed. 
                    To do this, the computer will help you with hints (Cold, warm, hot)""",
    "confirmations":{
        "range":"""Accepted!
                    I'm guessing a number within"""
    },
    "requests":{
        "range":"""Enter the range within which the computer will guess the numbers.
                    Specify 2 numbers separated by spaces. 
                    The first must be the minimum. 
                    The minimum must be less than the maximum.""",
        "guess":"""Enter your guess within the limits you set at the beginning of the game.
                    These limits are: """,
        "again":"""Play again?
                    Enter "n" to exit the game, and "y" to play again"""
    },
    "errors":{
        "range":"""I expect only 2 numbers from you, minimum and maximum""",
        "guess":"""I expect only 1 numbers from you""",
        "invalid_input":"""invalid input in current context""",
        "out_of_range":"""Number is out of range"""
    },
    "messages":{
        "win":"Congratulations! You've won!",
        "cold":"It's very cold, keep looking!",
        "warm":"Warmer, keep searching!",
        "hot":"Hot! You're so close!"
    }
}

if(__name__ == "__main__"):
    print(check_input(True).answer_data)