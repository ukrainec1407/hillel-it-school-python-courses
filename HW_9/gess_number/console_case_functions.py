import time
import keyboard

if __name__ == "__main__":
    import basic_functions as bsc_func
else:
    import gess_number.basic_functions as bsc_func
# Придумал такой костыль, как добавить файл, что бы он работал в коде домашки, но как правильно сделать?
# Мой вариант не будет работать в других случаях, кроме этого.

def func_time(func):
    """
        This function is decorator, which count and print time of function (func parameter) execution
    """
    def wrapper():
        st = time.time()
        result = func()
        et = time.time()
        elapsed_time = et - st
        print('Execution time:', elapsed_time, 'seconds')
        return result
    return wrapper
def request_range():
    """
        This function is requesting the range from user, describing instruction before asking

        :return: list with range (min and max value)
        :rtype: list with range (min and max value)
    """
    input_data = input(f'{bsc_func.messages["requests"]["range"]}\n')
    input_data = bsc_func.check_input(input_data)
    if(len(input_data.errors) > 0):
        for error in input_data.errors:
            print(error)
        return False
    elif(len(input_data.answer_data) != 2):
        print(bsc_func.messages["errors"]["range"])
        return False
    elif(input_data.answer_data[0] >= input_data.answer_data[1]):
            print(bsc_func.messages["errors"]["range"])
            return False
    return input_data.answer_data

def request_guess(target_number, range):
    """
        This function is requesting the users guessed number, describing instruction before asking,
        and compare entered number with target number.
        Also this funtion prints a tips for user, based on his number

        :param: target_number: number, which computer generated for guessing
        :type: target_number: int

        :param: range: list with 2 elements (min value of range, and max value)
        :type: range: list (min value of range, and max value)

        :return: if user win - True, if not False
        :rtype: bool
    """
    users_guess = input(f'{bsc_func.messages["requests"]["guess"]}{range}\n')
    users_guess = bsc_func.check_input(users_guess)
    if (len(users_guess.errors) > 0):
        for error in users_guess.errors:
            print(error)
        return False
    elif (len(users_guess.answer_data) > 1):
        print(bsc_func.messages["errors"]["guess"])
        return False
    else:
        result = bsc_func.check_number(users_guess.answer_data[0], target_number, range)
        if (len(result.errors) > 0):
            for error in result.errors:
                print(error)
            return False
        else:
            print(result.answer_data[0])
            if(result.answer_data[0] == bsc_func.messages["messages"]["win"]):
                return True
            return False

def request_agan():
    """
        This function is asking if user wants to play agan.
        Function works with keyboard, and uses functions from "keyboard" library

        :return: if user pressed y(yes) - False, if user pressed n(no) - True
        :rtype: bool
    """
    print(bsc_func.messages["requests"]["again"])

    while True:
        if keyboard.is_pressed('n'):
            return True
        elif keyboard.is_pressed('y'):
            return False

@func_time
def game():
    """
        Function which consist of all game logic.
        To start game - call this function
    """
    print(bsc_func.messages["greetings"])

    while True:
        range = False
        while not range:
            range = request_range()
        print(f'{bsc_func.messages["confirmations"]["range"]} {range}')
        target_number = bsc_func.set_number(range).answer_data[0]
        # print(target_number)
        user_number = request_guess(target_number, range)
        while not user_number:
            user_number = request_guess(target_number, range)

        if(request_agan()):
            break

if __name__ == "__main__":
    game()