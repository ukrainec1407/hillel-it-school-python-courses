def is_string_capitalized(input_string: str):
    """
    Function check if the given string is capitalised.
    Numbers always True.
    Empty string always True.
    Upper letters inside of string return False.

    Args:
        str: string to check on capitalised

    Returns:
        bool: if string is capitalised - True, if not False
    """
    if (not isinstance(input_string, str)):
        raise Exception('only STR type parameters are allowed')
    input_string = ' '.join(input_string.split())
    if (len(input_string) == 0):
        return True
    if (input_string.isdigit()):
        return True
    if ((input_string[0].isupper() and not (input_string[0].isdigit()))
            or (input_string[0].isdigit())):
        for char in input_string[1:]:
            if (input_string.isdigit()):
                continue
            if (char.isupper()):
                return False
        return True
    else:
        return False


def is_odd(number: int | float | str):
    """
        This function check number on odd and even.


        :param: number: number to check if it odd or even
        :type: number: int | float

        :return: if number is odd - True, if not False
        :rtype: bool
    """

    if (not isinstance(number, (int, float, str))):
        raise Exception('only INT, FLOAT or STR parameters are allowed')
    try:
        number = float(number)
    except ValueError:
        raise Exception('STR parameter is not a number')

    if (number % 2) == 0:
        return True
    else:
        return False
