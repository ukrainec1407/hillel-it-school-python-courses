import library

assert library.is_string_capitalized('My name is David') is False
assert library.is_string_capitalized('I love playing') is True
assert library.is_string_capitalized('') is True
assert library.is_string_capitalized('565656') is True
assert library.is_string_capitalized('   ') is True
assert library.is_string_capitalized('   000') is True
assert library.is_string_capitalized('   A') is True


for number in range(-15, 15):
    if (number % 2) != 0:
        continue
    if (-5 < number < 5):
        assert library.is_odd(str(number)) is True
    else:
        assert library.is_odd(number) is True

even_numbers = [1, -1, 5, -5, 2.2, -1.2, "1", "-1", "-2.1", "-1.1"]

for even_number in even_numbers:
    assert library.is_odd(even_number) is False
